> A simple static webserver, as an application

* * *

## Getting Started

### Build the app from the source

You can build the app by yourself:

1. Clone the repository
2. Run `npm install` to get the dependencies
3. Run `grunt` to build & run the app
4. Use `grunt release` if you want to build a binary.


### TODO

* [x] Allow to drag'n'drop a folder on window to set the server root
* [x] Auto-try to resolve url like `page` to `page.html`
* [x] Custom 404 page
* [x] Multiple-window feature, managing multiple servers
* [ ] About window
* [x] Refactor repo structure + builder
* [ ] All-around refactor
* [ ] Implement main menu for OSX & Windows
* [x] Test Windows
* [x] Test Linux
* [x] Presentation website

## Release History
* **2016/06/03:** `0.5.3` fix "choose folder" button
* **2016/06/03:** `0.5.2` fix build process, causing missing dependencies in final app
* **2016/06/02:** `0.5.1` fix issues & deprecations from Electron 1.0 update
* **2016/05/08:** `0.5.0` multiple-window managment
* **2015/09/21:** `0.4.0` 404 managment, auto-try to resolve url
* **2015/09/11:** `0.3.0` drag'n'drop feature, new build system
* **2015/09/06:** `0.2.0` autoindex feature
* **2015/08/26:** `0.1.0` basic features
* **2015/08/25:** starting project
